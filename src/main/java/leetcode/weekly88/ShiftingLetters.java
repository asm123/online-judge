package leetcode.weekly88;

public class ShiftingLetters {

    public static void main(String[] args) {
        String S = "abc";
        int[] shifts = {3, 5, 9};
        System.out.println(new Solution().shiftingLetters(S, shifts));
    }

    static class Solution {
        public String shiftingLetters(String S, int[] shifts) {
            char[] s = S.toCharArray();
            for (int i = shifts.length-1; i >= 0; i--) {
                if (i < shifts.length-1) {
                    shifts[i] = (shifts[i] + shifts[i+1]) % 26;
                }
                s[i] = (char) (((s[i] - 'a') + shifts[i]) % 26 + 'a');
            }
            return new String(s);
        }
    }
}

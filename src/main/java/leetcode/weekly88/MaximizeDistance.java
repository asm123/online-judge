package leetcode.weekly88;

import java.util.Arrays;

public class MaximizeDistance {

    public static void main(String[] args) {
        int[] seats = {1,0,0,0};
        System.out.println(new Solution().maxDistToClosest(seats));
    }

    static class Solution {
        public int maxDistToClosest(int[] seats) {
            int[] forward = new int[seats.length];
            int[] backward = new int[seats.length];
            for (int i = 0; i < forward.length; i++) {
                if (seats[i] == 1) {
                    forward[i] = 0;
                } else {
                    if (i > 0) {
                        forward[i] = forward[i-1] + 1;
                    } else {
                        forward[i] = forward.length-1;
                    }
                }
            }
            for (int i = backward.length-1; i >= 0; i--) {
                if (seats[i] == 1) {
                    backward[i] = 0;
                } else {
                    if (i < backward.length-1) {
                        backward[i] = backward[i+1] + 1;
                    } else {
                        backward[i] = backward.length-1;
                    }
                }
            }
//            System.out.println(Arrays.toString(forward));
//            System.out.println(Arrays.toString(backward));
            int max = 0;
            for (int i = 0; i < forward.length; i++) {
                max = Math.max(max, Math.min(forward[i], backward[i]));
            }
            return max;
        }
    }
}

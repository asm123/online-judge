package leetcode.weekly88;

import java.util.*;

public class LoudAndRich {

    public static void main(String[] args) {
        int[][] richer = {{1,0},{2,1},{3,1},{3,7},{4,3},{5,3},{6,3}};
        int[] quiet = {3,2,5,4,6,1,7,0};
        System.out.println(Arrays.toString(new Solution().loudAndRich(richer, quiet)));
    }

    static class Solution {
        public int[] loudAndRich(int[][] richer, int[] quiet) {
            int[] answer =  new int[quiet.length];
            for (int i = 0; i < answer.length; i++) {
                answer[i] = i;
            }
            List<List<Integer>> adj = new ArrayList<>();
            for (int i = 0; i < quiet.length; i++) {
                adj.add(new ArrayList<>());
            }
            for (int[] r : richer) {
                adj.get(r[1]).add(r[0]);
            }
            boolean[] visited = new boolean[quiet.length];
            for (int i = 0; i < quiet.length; i++) {
                dfs(i, adj, quiet, answer, visited);
            }
            return answer;
        }

        private void dfs(int u, List<List<Integer>> adj, int[] quiet, int[] answer, boolean[] visited) {
            visited[u] = true;
            for (int v: adj.get(u)) {
                if (!visited[v]) {
                    dfs(v, adj, quiet, answer, visited);
                }
                if (quiet[answer[v]] < quiet[answer[u]]) {
                    answer[u] = answer[v];
                }
            }
        }
    }
}

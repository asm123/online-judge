package leetcode.practice;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class LetterCasePermutation {

    public static void main(String[] args) {
        String input = "a1b2";
        System.out.println(new Solution().letterCasePermutation(input));
    }

    static class Solution {
        public List<String> letterCasePermutation(String S) {
            List<String> permutations = new ArrayList<>();
            List<Character> letters = new ArrayList<Character>();
            for (int i = 0; i < S.length(); i++) {
                if (Character.isAlphabetic(S.charAt(i))) {
                    letters.add(S.charAt(i));
                }
            }
            if (letters.isEmpty()) {
                permutations.add(S);
            } else {
                List<String> binary = getBinaryCombinations(letters.size());
                for (String b : binary) {
                    StringBuilder sb = new StringBuilder();
                    int k = 0;
                    for (int i = 0; i < S.length(); i++) {
                        char c = S.charAt(i);
                        if (Character.isDigit(c)) {
                            sb.append(c);
                        } else {
                            if (b.charAt(k) == '0') {
                                sb.append(Character.toLowerCase(c));
                            } else {
                                sb.append(Character.toUpperCase(c));
                            }
                            k++;
                        }
                    }
                    permutations.add(sb.toString());
                }
            }
            return permutations;
        }

        private List<String> getBinaryCombinations(int n) {
            List<String> binary = new ArrayList<>();
            int max = 1 << n;
            for (int i = 0; i < max; i++) {
                binary.add(toBinary(i, n));
            }
            return binary;
        }

        private String toBinary(int n, int numberOfBits) {
            StringBuilder sb = new StringBuilder(Integer.toBinaryString(n)).reverse();
            while (sb.length() != numberOfBits) {
                sb.append("0");
            }
            return sb.reverse().toString();
        }
    }
}

package leetcode.practice;

import java.util.*;

public class IterativePostorder {


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    static class Solution {
        public List<Integer> postorderTraversal(TreeNode root) {
            List<Integer> traversal = new ArrayList<>();

            Map<TreeNode, TreeNode> parent = new HashMap<>();
            Map<TreeNode, Boolean> visited = new HashMap<>();

            Stack<TreeNode> stack = new Stack<>();
            stack.push(root);
            parent.put(root, null);
            visited.put(root, false);

            while (!stack.empty()) {
                TreeNode top = stack.peek();

                if (visited.get(top)) {
                    stack.pop();
                    traversal.add(top.val);
                } else {
                    if (top.left == null && top.right == null) {
                        if (top != root) {
                            TreeNode parentNode = parent.get(top);
                            visited.put(parentNode, true);
                        }
                    } else {
                        if (top.right != null) {
                            stack.push(top.right);
                            parent.put(top.right, top);
                            visited.put(top.right, false);
                        }
                        if (top.left != null) {
                            stack.push(top.left);
                            parent.put(top.left, top);
                            visited.put(top.left, false);
                        }
                    }
                    visited.put(top, true);
                }
            }

            return traversal;
        }
    }
}

package leetcode.practice;


import java.util.HashMap;

public class LFUCache {
    class Node {
        private int val;
        private int freq;
        private Node next;
        private Node prev;
        private int key;

        public Node(int key, int val) {
            this.val = val;
            this.key = key;
            this.freq = 1;
            next = null;
            prev = null;
        }


        public void setFreq(int count) {
            this.freq = count;
        }

        public void setPrevious(Node previous) {
            this.prev = previous;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public int getKey() {
            return this.key;
        }

        public int getValue() {
            return this.val;
        }

        public Node getPrevious() {
            return this.prev;
        }

        public Node getNext() {
            return this.next;
        }

        public int getCount() {
            return this.freq;
        }

        public void increment() {
            this.freq++;
        }

        public void setValue(int value) {
            this.val = value;
        }

    }

    int size;
    int capacity;

    HashMap<Integer, Node> NodeChecker;
    HashMap<Integer, Node> FreqCounter;

    Node head;
    Node tail;

    public LFUCache(int capacity) {

        head = null;
        tail = null;
        size = 0;
        this.capacity = capacity;

        NodeChecker = new HashMap<Integer, Node>();
        FreqCounter = new HashMap<Integer, Node>();
    }

    public void nodeLinksSetup(Node node) {
        if (node.next != null) {
            node.next.prev = node.prev;
        }
        if (node.prev != null) {
            node.prev.next = node.next;
        }
        node.next = null;
        node.prev = null;

    }

    public int get(int key) {
        if (capacity <= 0 || !NodeChecker.containsKey(key))
            return -1;


        Node node = NodeChecker.get(key);
        int newFreq = node.freq + 1;


        Node currFreqRecent = FreqCounter.get(node.freq);
        Node newFreqRecent = FreqCounter.get(node.freq + 1);

        //currFreqRecent can not be null. It can be either node or something different
        if (currFreqRecent == node) {
            if (node.prev != null && node.prev.freq == node.freq)
                FreqCounter.put(node.freq, node.prev);
            else
                FreqCounter.remove(node.freq);

            if (head == node) {
                //it means we need to move node when its frequency count changes and append it next to newFreqRecent
                if (newFreqRecent != null) {
                    head = node.next;

                    if (head != null)
                        head.prev = null;

                    node.next = newFreqRecent.next;
                    node.prev = newFreqRecent;
                    newFreqRecent.next = node;
                    FreqCounter.put(newFreq, node);
                }
                //there is no elemnt of new frequency, so head will remain same, node will change frequency but not change position
                else {
                    FreqCounter.put(newFreq, node);
                }

            }
            // node is not @ head position but its recent most element of given frequency
            else {
                //if newFreqRecent is null, node will change only frequency but not position
                if (newFreqRecent == null) {
                    FreqCounter.put(newFreq, node);
                } else {
                    if (node.prev != null) {
                        node.prev.next = node.next;
                    }
                    if (node.next != null) {
                        node.next.prev = node.prev;
                    }
                    FreqCounter.put(newFreq, node);
                    node.next = newFreqRecent.next;
                    node.prev = newFreqRecent;
                    newFreqRecent.next = node;
                }
            }
        }
        //node is in between head and curFreqRecent. need to adjust pointer and move node
        else {

            //create recent elemnt with nodes freqency
            if (newFreqRecent != null) {
                FreqCounter.put(newFreq, node);
                if (node == head) {
                    head = head.next;
                    head.prev = null;
                } else {
                    if (node.prev != null)
                        node.prev.next = node.next;

                    if (node.next != null)
                        node.next.prev = node.prev;
                }
                node.prev = newFreqRecent;
                node.next = newFreqRecent.next;
                newFreqRecent.next = node;
                if (node.next != null) {
                    node.next.prev = node;
                }
            }
            //newFreqRecent is null, so add elemnt after currFreqRecent
            else {
                FreqCounter.put(newFreq, node);
                if (node == head) {
                    head = head.next;
                    head.prev = null;
                } else {
                    if (node.prev != null)
                        node.prev.next = node.next;

                    if (node.next != null)
                        node.next.prev = node.prev;
                }
                node.prev = currFreqRecent;
                node.next = currFreqRecent.next;
                currFreqRecent.next = node;
                if (node.next != null) {
                    node.next.prev = node;
                }

            }
        }
        return node.val;


    }

    public void put(int key, int value) {

        if (capacity <= 0)
            return;

        if (NodeChecker.containsKey(key) && (NodeChecker.get(key).val != value)) {
            Node existing = NodeChecker.get(key);
            //if(existing.val != value)

            existing.val = value;
            this.get(key);

            return;
        }

        Node newNode = new Node(key, value);
        //NodeChecker.put(key,newNode);
        if (head == null && (size + 1) <= capacity) {
            head = newNode;
            FreqCounter.put(1, newNode);
            NodeChecker.put(key, newNode);
            size++;
        } else {
            //if there is space to add one more element
            if (size + 1 <= capacity) {
                //Case: At present cache contains only one elemnt.
                if (NodeChecker.size() == 1) {
                    //Subcase 1: Frequency of use of elemnt present is 1, head need not be updated
                    if (head.freq == 1) {
                        head.next = newNode;
                        newNode.prev = head;
                        FreqCounter.put(1, newNode);
                    }
                    //Subcase 1: Frequency of use of elemnt present is greater than 1, head needs to be updated
                    else {
                        FreqCounter.put(1, newNode);
                        head.prev = newNode;
                        newNode.next = head;
                        head = newNode;
                    }
                    NodeChecker.put(key, newNode);
                }
                //There are multiple elements in cache and we wish to insert new elemnt
                else {
                    Node temp = FreqCounter.get(1);

                    //no element of frequency 1, make new element as head
                    if (temp == null) {
                        FreqCounter.put(1, newNode);
                        head.prev = newNode;
                        newNode.next = head;
                        head = newNode;
                    }
                    //add newNode as adjacent to this freq 1 node. update newNode as current last node of frequency 1
                    else {
                        newNode.next = temp.next;
                        newNode.prev = temp;
                        temp.next = newNode;
                        if (newNode.next != null) {
                            newNode.next.prev = newNode;
                        }
                        FreqCounter.put(1, newNode);
                    }
                    NodeChecker.put(key, newNode);
                    //using frequency map we can last inserted element of frequency one, and insert new element next to it. Thanks to Asmita for suggesting this
                    //This will prevent while loop, which was  method i used earlier. It is slow way and causes TLE
                    /*if(head != null && head.freq == 1)
                    {
                        Node temp = head;
                        while(temp.next != null && temp.next.freq ==1)
                        {
                            temp = temp.next;
                        }
                        if(temp.next == null)
                        {
                            temp.next = newNode;
                            newNode.prev = temp;
                        }
                        else{
                            newNode.next = temp.next;
                            newNode.prev = temp;
                            newNode.next.prev = newNode;
                            newNode.prev.next = newNode;
                        }

                    }
                    else{
                        if(tail != null)
                            tail.prev = newNode;

                        newNode.next = tail;
                        tail = newNode;
                    }*/
                }
                size++;

            }
            //cache is full
            //delete least frequently used cache entry, which is at head, add new entry at next to frequency entry;
            else {
                //cache is of size 1 and cache alreay has 1 element, head will be assigned to null before inserting elemnt
                if (NodeChecker.size() == 1) {
                    NodeChecker.remove(head.key);
                    FreqCounter.remove(head.freq);
                    head = null;
                    size--;

                    this.put(key, newNode.val);
                }
                //cache has more than 1 element and we need to remove element at head
                else {
                    NodeChecker.remove(head.key);
                    if (FreqCounter.get(head.freq) == head) {
                        FreqCounter.remove(head.freq);
                    }
                    head = head.next;
                    if (head != null)
                        head.prev = null;

                    size--;
                    this.put(key, newNode.val);
                }
            }
        }

    }

    public static void main(String[] args) throws Exception {
        LFUCache cache = new LFUCache(10);
        /*cache.put(3,1);
        cache.put(2,1);
        cache.put(2,2);
        cache.put(4,4);

        System.out.println(cache.get(2));
        */


        cache.put(10, 13);
        cache.put(3, 17);
        cache.put(6, 11);
        cache.put(10, 5);
        cache.put(9, 10);
        System.out.println(cache.get(13));
        cache.put(2, 19);
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        cache.put(5, 25);
        System.out.println(cache.get(8));
        cache.put(9, 22);
        cache.put(5, 5);
        cache.put(1, 30);
        System.out.println(cache.get(11));
        cache.put(9, 12);
        System.out.println(cache.get(7));
        System.out.println(cache.get(5));
        System.out.println(cache.get(8));
        System.out.println(cache.get(9));
        cache.put(4, 30);
        cache.put(9, 3);
        System.out.println(cache.get(9));
        System.out.println(cache.get(10));
        System.out.println(cache.get(10));
        cache.put(6, 14);
        cache.put(3, 1);
        System.out.println(cache.get(3));
        cache.put(10, 11);
        System.out.println(cache.get(8));
        cache.put(2, 14);
        System.out.println(cache.get(1));
        System.out.println(cache.get(5));
        System.out.println(cache.get(4));
        cache.put(11, 4);
        cache.put(12, 24);
        cache.put(5, 18);
        System.out.println(cache.get(13));
        cache.put(7, 23);
        System.out.println(cache.get(8));
        System.out.println(cache.get(12));
        cache.put(3, 27);
        cache.put(2, 12);
        System.out.println(cache.get(5));
        cache.put(2, 9);
        cache.put(13, 4);
        cache.put(8, 18);
        cache.put(1, 7);
        System.out.println(cache.get(6));
        cache.put(9, 29);
        cache.put(8, 21);
        System.out.println(cache.get(5));
        cache.put(6, 30);
        cache.put(1, 12);
        System.out.println(cache.get(10));







        /*System.out.println(cache.get(1));
        System.out.println(cache.get(2));
        cache.put(3,3);
        cache.put(4,4);
        System.out.println(cache.get(3));
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        System.out.println(cache.get(4));
*/
/*
        //Base testcase
        cache.put(1, 1);
        cache.put(2, 2);
        System.out.println(cache.get(1));       // returns 1
        cache.put(3, 3);    // evicts key 2
        System.out.println(cache.get(2));       // returns -1 (not found)
        System.out.println(cache.get(3));      // returns 3.
        cache.put(4, 4);    // evicts key 1.
        System.out.println(cache.get(1));// returns -1 (not found)
        System.out.println(cache.get(3));// returns 3
        System.out.println(cache.get(4));// returns 4
*/
    }
}



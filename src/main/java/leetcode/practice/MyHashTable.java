package leetcode.practice;

import java.util.LinkedList;
import java.util.List;

public class MyHashTable<K, V> {
    private List[] table;
    private static final int MAX_SIZE = 1024;

    public MyHashTable() {
        table = new LinkedList[MAX_SIZE];
        for (int i = 0; i < table.length; i++) {
            table[i] = new LinkedList<Node<K, V>>();
        }
    }

    private int hash(K key) {
        return key.toString().length() % MAX_SIZE;
    }

    public void put(K key, V value) {
        int hash = hash(key);
        List<Node<K, V>> existing = table[hash];
        for (Node<K, V> node : existing) {
            if (node.getKey().equals(key)) {
                existing.remove(node);
                break;
            }
        }
        Node<K, V> newNode = new Node<>(key, value);
        existing.add(newNode);
    }

    public V get(K key) {
        int hash = hash(key);
        List<Node<K, V>> existing = table[hash];
        for (Node<K, V> node : existing) {
            if (node.getKey().equals(key)) {
                return node.getValue();
            }
        }
        return null;
    }

    public boolean containsKey(K key) {
        int hash = hash(key);
        List<Node<K, V>> existing = table[hash];
        for (Node<K, V> node : existing) {
            if (node.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public void remove(K key) {
        int hash = hash(key);
        List<Node<K, V>> existing = table[hash];
        for (Node<K, V> node : existing) {
            if (node.getKey().equals(key)) {
                existing.remove(node);
                return;
            }
        }
    }

    private class Node<K, V> {
        private K key;
        private V value;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        K getKey() {
            return key;
        }

        V getValue() {
            return value;
        }
    }

    public static void main(String[] args) {
        MyHashTable<Integer, Integer> hashTable = new MyHashTable<>();
        hashTable.put(23, 100);
        System.out.println(hashTable.get(23));
        hashTable.put(23, 200);
        System.out.println(hashTable.get(23));
        hashTable.remove(23);
        System.out.println(hashTable.get(23));
    }


}




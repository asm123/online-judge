package leetcode.practice;

import java.util.*;
import java.util.stream.Collectors;

public class GenerateParanthesis {

    public static void main(String[] args) {
        int n = 3;
        List<String> parantheses = new Solution().generateParenthesis(n);
        for (String p : parantheses) {
            System.out.println(p);
        }
    }

    static class Solution {
        public List<String> generateParenthesis(int n) {
            if (n == 0) {
                return new ArrayList<>();
            }
            if (n == 1) {
                return Collections.singletonList("()");
            }
            List<String> left = generateParenthesis(n - 1);
            Set<String> all = new HashSet<>();
            for (String s : left) {
                for (int i = 0; i < s.length(); i++) {
                    all.add(addAtPosition(s, i));
                }
            }
            return all.stream().collect(Collectors.toList());
        }

        private String addAtPosition(String s, int position) {
            return s.substring(0, position) + "()" + s.substring(position);
        }
    }
}

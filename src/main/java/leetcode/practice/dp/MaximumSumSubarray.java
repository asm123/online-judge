package leetcode.practice.dp;

public class MaximumSumSubarray {

    public static void main(String[] args) {
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        System.out.println(new Solution().maxSubArray(nums));
    }

    static class Solution {
        public int maxSubArray(int[] nums) {
            int max = nums[0];
            int prevSum = nums[0];
            for (int i = 1; i < nums.length; i++) {
                int newSum = nums[i];
                if (prevSum > 0) {
                    newSum += prevSum;
                }
                max = Math.max(max, newSum);
                prevSum = newSum;
            }

            return max;
        }
    }
}

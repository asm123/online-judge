package leetcode.practice;

public class MaxXOR {

    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
            left = null;
            right = null;
        }

    }

    public static void main(String[] args) {

        MaxXOR obj = new MaxXOR();
        int nums[] = {3, 10, 5, 25, 2, 8};
//        int nums[] = {1, 2};
        int op = obj.findMaximumXOR(nums);
        System.out.println(op);
    }

    public int findMaximumXOR(int[] nums) {

        TreeNode root = new TreeNode(-1);

        int max = 0;
        generteTrie(nums, root);
        TravelTree(root);
        max = getMaxXOR(root, root, 0, 31);
        return max;
    }

    private void TravelTree(TreeNode root) {
        if (root == null)
            return;
        if (root.left != null) {
            TravelTree(root.left);
        }
        if (root.right != null) {
            TravelTree(root.right);
        }
    }


    public void generteTrie(int[] nums, TreeNode rootOrig) {
        for (int i : nums) {
            TreeNode root = rootOrig;
            for (int j = 31; j >= 0; j--) {
                if ((i & (1 << j)) > 0) {
                    if (root.right == null)
                        root.right = new TreeNode(1);
                    root = root.right;
                } else {
                    if (root.left == null)
                        root.left = new TreeNode(0);

                    root = root.left;
                }
            }

        }
    }

    public int getMaxXOR(TreeNode left, TreeNode right, int sum, int index) {
        if (index == -1) {
            return sum;
        }
        if (left == right) {
            if (left.left != null && right.right != null) {
                return getMaxXOR(left.left, right.right, sum + (int) ((Math.pow(2, index))), index - 1);
            } else if (left.left == null) {
                return getMaxXOR(left.right, left.right, sum, index - 1);
            } else {
                return getMaxXOR(left.left, left.left, sum, index - 1);
            }
        } else {
            int lr = 0, ll = 0, rr = 0, rl = 0;
            if (left.left != null && right.right != null)
                lr = getMaxXOR(left.left, right.right, (int) (sum + (Math.pow(2, index))), index - 1);


            if (left.right != null && right.left != null)
                rl = getMaxXOR(left.right, right.left, (int) (sum + (Math.pow(2, index))), index - 1);


            if (left.left != null && right.left != null) {
                ll = getMaxXOR(left.left, right.left, sum, index - 1);
            }
            if (right.right != null && left.right != null) {
                rr = getMaxXOR(left.right, right.right, sum, index - 1);
            }
            return Math.max(Math.max(lr, rl), Math.max(rr, ll));
        }

    }

}




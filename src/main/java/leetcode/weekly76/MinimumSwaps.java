package leetcode.weekly76;

public class MinimumSwaps {

    public static void main(String[] args) {
        int[] A = {14, 18, 19, 20, 20};
        int[] B = {17, 15, 16, 19, 21};
        System.out.println(new Solution().minSwap(A, B));
    }

    static class Solution {

        static int swapC[];

        public static boolean isStrictlyIncreasing(int arr[], int index) {
            for (int i = index; i < arr.length - 1; i++) {
                if (arr[i] >= arr[i + 1])
                    return false;
            }
            return true;
        }

        public static int minSwapR(int[] A, int[] B, int index, int swapCount) {

            if (swapC[index] != Integer.MAX_VALUE) {
                return swapC[index];
            }

            if (isStrictlyIncreasing(A, 0) && isStrictlyIncreasing(B, 0)) {
                // swapC[index][swapCount] = swapCount;
                return swapCount;//[index][swapCount];
            } else {
                if (index == A.length || swapCount > A.length) {
                    return Integer.MAX_VALUE;
                } else {

                    int excludingSwap = minSwapR(A, B, index + 1, swapCount);

                    int temp = A[index];
                    A[index] = B[index];
                    B[index] = temp;

                    int includingSwap = minSwapR(A, B, index + 1, swapCount + 1);

                    swapC[index] = Math.min(excludingSwap, includingSwap);

                    temp = A[index];
                    A[index] = B[index];
                    B[index] = temp;

                    return swapC[index];
                }
            }
        }

        public static int minSwap(int[] A, int[] B) {
            int swapCount = 0;
            swapC = new int[A.length + 1];

            for (int i = 0; i <= A.length; i++) {
                swapC[i] = Integer.MAX_VALUE;

            }
            swapC[0] = minSwapR(A, B, 0, 0);
            //swapCount = minSwapR( A,  B, 0,0);
            return swapC[0];
        }

    }
}

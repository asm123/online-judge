package leetcode.weekly76;

public class SimilarRGBColor {

    public static void main(String[] args) {
        String color = "#4e3fe1";
        System.out.println(new Solution().similarRGB(color));
    }

    static class Solution {
        public String similarRGB(String color) {
            return "#" + getMostSimilar(color.substring(1,3)) + getMostSimilar(color.substring(3,5)) +  getMostSimilar(color.substring(5));
        }

        private String getMostSimilar(String color) {
            String mostSimilar = "";

            int dec = hexToDecimal(color);
            int remainder = dec % 17;
            if (remainder == 0) {
                return color;
            }
            int quotient = dec / 17;
            if (remainder >= 9) {
                quotient++;
            }
            if (quotient <= 9) {
                mostSimilar = "" + quotient + "" + quotient;
            } else {
                char hexChar = (char)('a' + (quotient - 10));
                mostSimilar = "" + hexChar + "" + hexChar;
            }
            return mostSimilar;
        }

        private int hexToDecimal(String hex) {
            int decimal = 0;
            if (hex.charAt(1) >= 'a' && hex.charAt(1) <= 'f') {
                decimal += (int)(hex.charAt(1) - 'a') + 10;
            } else {
                decimal += (int)(hex.charAt(1) - '0');
            }

            if (hex.charAt(0) >= 'a' && hex.charAt(0) <= 'f') {
                decimal += 16 * ((int)(hex.charAt(0) - 'a') + 10);
            } else {
                decimal += 16 * (int)(hex.charAt(0) - '0');
            }
            return decimal;
        }
    }
}

package leetcode.weekly84;

import java.util.HashSet;
import java.util.Set;

public class FindAndReplaceString {

    public static void main(String[] args) {
        String S = "ejvzndtzncrelhedwlwiqgdbdgctgubzczgtovufncicjlwsmfdcrqeaghuevyexqdhffikvecuazrelofjmyjjznnjdkimbklrh";
        int[] indexes = {25,35,60,77,69,79,15,19,58,92,27,64,4,11,51,7,72,67,30,0,74,98,17,85,48,32,38,62,43,2,9,55,87};
        String[] sources = {"gc","tov","vy","re","ikv","lo","dw","iqgdbd","ue","kimbk","tgu","qd","ndt","elhe","crq","zn","ec","ff","bz","ej","ua","rh","lw","jj","mfd","cz","ufn","ex","cjl","vz","cr","agh","znnj"};
        String[] targets = {"dxs","hn","vfc","wnr","tira","rko","oob","mlitiwj","zrj","onpp","ot","c","lm","hbsje","dgc","ruf","qi","h","vzn","ja","ow","te","km","imq","pia","ixp","xsod","m","eat","yf","lzu","dgy","dyrsc"};
        System.out.println(new Solution().findReplaceString(S, indexes, sources, targets));
    }

    static class Solution {
        public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
            StringBuffer last = new StringBuffer(S);
            for (int i = 0; i < indexes.length; i++) {
                Set<Integer> allIndexes = getAllIndexesOf(S, sources[i]);
                if (allIndexes.contains(indexes[i])) {
                    int currentIndex = last.indexOf(sources[i]);
                    if (currentIndex != -1) {
                        last = new StringBuffer(last.substring(0, currentIndex))
                                .append(targets[i])
                                .append(last.substring(currentIndex + sources[i].length()));

                    }
                }
            }
            return last.toString();
        }

        private Set<Integer> getAllIndexesOf(String S, String s) {
            Set<Integer> indexes = new HashSet<>();
            int i = 0;
            int start = 0;
            while (start < S.length()) {
                int index = S.substring(start).indexOf(s);
                if (index == -1) {
                    break;
                }
                indexes.add(start + index);
                start += index + s.length();
            }
            return indexes;
        }
    }
}

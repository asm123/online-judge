package leetcode.weekly81;

import java.util.Arrays;
import java.util.Comparator;

public class ShortEncoding {

    public static void main(String[] args) {
        String[] words = {"me", "time", "alltime", "sometime"};
        System.out.println(new Solution().minimumLengthEncoding(words));
    }

    static class Solution {
        public int minimumLengthEncoding(String[] words) {
            if (words.length == 0) {
                return 0;
            }
            Comparator<String> lengthComparator = (o1, o2) -> Integer.compare(o2.length(), o1.length());
            Arrays.sort(words, lengthComparator);
            StringBuilder encoded = new StringBuilder();
            for (String word: words) {
                String temp = word + "#";
                if (encoded.indexOf(temp) == -1) {
                    encoded.append(temp);
                }
            }
//            System.out.println(encoded);
            return encoded.length();
        }
    }
}

package leetcode.weekly81;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class CardFlipping {

    public static void main(String[] args) {
        int[] fronts = {1, 2};
        int[] backs = {2, 1};
        System.out.println(new Solution().flipgame(fronts, backs));
    }

    static class Solution {
        public int flipgame(int[] fronts, int[] backs) {
            Set<Integer> uselesss = new HashSet<>();
            Set<Integer> useful = new TreeSet<>();
            for (int i = 0; i < fronts.length; i++) {
                if (fronts[i] == backs[i]) {
                    uselesss.add(fronts[i]);
                } else {
                    useful.add(fronts[i]);
                    useful.add(backs[i]);
                }
            }
            for (int i: useful) {
                if (!uselesss.contains(i)) {
                    return i;
                }
            }
            return 0;
        }
    }
}

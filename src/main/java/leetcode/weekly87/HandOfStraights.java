package leetcode.weekly87;

import java.util.*;

public class HandOfStraights {

    public static void main(String[] args) {
        int[] hand = {1,2,3,6,2,3,4,7,8};
        int W = 3;
        System.out.println(new Solution().isNStraightHand(hand, W));
    }

    static class Solution {
        public boolean isNStraightHand(int[] hand, int W) {
            Map<Integer, Integer> counts = new HashMap<>();
            for (int n : hand) {
                counts.put(n, counts.getOrDefault(n, 0) + 1);
            }
            TreeSet<Integer> uniqueSet = new TreeSet<>(counts.keySet());
            while (uniqueSet.size() >= W) {
                int i = 0;
                int last = -1;
                Set<Integer> toRemove = new HashSet<>();
                for (int current : uniqueSet) {
                    if (i == W) {
                        break;
                    }
                    if (last > 0 && current != last + 1) {
                        return false;
                    }
                    last = current;
                    int currentCount = counts.get(current)-1;
                    if (currentCount == 0) {
                        counts.remove(current);
                        toRemove.add(current);
                    } else {
                        counts.put(current, counts.get(current)-1);
                    }
                    i++;
                }
                uniqueSet.removeAll(toRemove);
            }
            return uniqueSet.isEmpty();
        }
    }
}

package leetcode.weekly87;

import java.util.Arrays;

public class LongestMountain {

    public static void main(String[] args) {
        int[] a = {2,3,3,2,0,2};
        System.out.println(new Solution().longestMountain(a));
    }


    static class Solution {
        public int longestMountain(int[] a) {
            if (a == null || a.length == 0) {
                return 0;
            }
            int[] inc = new int[a.length];
            int[] dec = new int[a.length];
            Arrays.fill(inc, 1);
            Arrays.fill(dec, 1);

            for (int i = 1; i < a.length; i++) {
                if (a[i] > a[i-1]) {
                    inc[i] = inc[i-1] + 1;
                }
            }

            for (int i = a.length-2; i >=0; i--) {
                if (a[i] > a[i+1]) {
                    dec[i] = dec[i+1] + 1;
                }
            }

//            System.out.println(Arrays.toString(a));
//            System.out.println(Arrays.toString(inc));
//            System.out.println(Arrays.toString(dec));
            int max = 0;
            for (int i = 0; i < a.length; i++) {
                if (inc[i] > 1 && dec[i] > 1) {
                    max = Math.max(max, inc[i] + dec[i] - 1);
                }
            }
            if (max == 1) {
                return 0;
            }
            return max;
        }
    }
}

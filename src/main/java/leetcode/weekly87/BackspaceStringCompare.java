package leetcode.weekly87;

public class BackspaceStringCompare {

    public static void main(String[] args) {
        String S = "a#c";
        String T = "b";
        System.out.println(new Solution().backspaceCompare(S, T));
    }

    static class Solution {
        public boolean backspaceCompare(String S, String T) {
            if (S.isEmpty() && T.isEmpty()) {
                return true;
            }

            String s = buildArray(S);
            String t = buildArray(T);
            return s.equals(t);
        }

        private String buildArray(String S) {
            char[] s = new char[201];
            int current = 0;
            for (int i = 0; i < S.length(); i++) {
                if (S.charAt(i) == '#') {
                    if (current > 0) {
                        current--;
                        s[current] = ' ';
                    }
                } else {
                    s[current] = S.charAt(i);
                    current++;
                }
            }
            return new String(s).trim().replace("\\s+", "");
        }
    }
}

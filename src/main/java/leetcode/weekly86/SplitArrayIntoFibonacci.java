package leetcode.weekly86;

import java.util.ArrayList;
import java.util.List;

public class SplitArrayIntoFibonacci {

    public static void main(String[] args) {
        String S = "214748364721474836422147483641";
        System.out.println(new Solution().splitIntoFibonacci(S));
    }

    static class Solution {
        public List<Integer> splitIntoFibonacci(String S) {
            if (S == null || S.isEmpty()) {
                return new ArrayList<>();
            }
            List<Integer> fib = new ArrayList<>();
            generateFib(S, fib, 0);
            return fib;
        }

        private boolean generateFib(String s, List<Integer> fib, int current) {
            if (current == s.length() && fib.size() > 2) {
                return true;
            }
            for (int i = current; i < s.length(); i++) {
                if (s.charAt(current) == '0' && i > current) {
                    return false;
                }
                String requiredSumStr = s.substring(current, i + 1);
                if (ignore(requiredSumStr)) {
                    return false;
                }
                int requiredSum = Integer.valueOf(requiredSumStr);
                if (fib.size() > 1 && fib.get(fib.size() - 1) + fib.get(fib.size() - 2) < requiredSum) {
                    return false;
                }
                if (fib.size() < 2 || fib.get(fib.size() - 1) + fib.get(fib.size() - 2) == requiredSum) {
                    fib.add(requiredSum);
                    if (generateFib(s, fib, i + 1)) {
                        return true;
                    }
                    fib.remove(fib.size() - 1);
                }
            }
            return false;
        }

        private boolean ignore(String s) {
            return s.charAt(0) == '0' && s.length() > 1 || Long.parseLong(s) >= Integer.MAX_VALUE;
        }

    }
}

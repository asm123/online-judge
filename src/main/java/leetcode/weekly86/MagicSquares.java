package leetcode.weekly86;

public class MagicSquares {

    public static void main(String[] args) {
        int[][] grid = {{10, 3, 5},
                {1, 6, 11},
                {7, 9, 2}};
        System.out.println(new Solution().numMagicSquaresInside(grid));
    }

    static class Solution {
        public int numMagicSquaresInside(int[][] grid) {
            int count = 0;
            for (int i = 0; i + 2 < grid.length; i++) {
                for (int j = 0; j + 2 < grid[0].length; j++) {
                    if (isMagicGrid(grid, i, j)) {
                        count++;
                    }
                }
            }
            return count;
        }

        private boolean isMagicGrid(int[][] grid, int rowStart, int colStart) {
            int[] rowSum = new int[3];
            int k = 0;
            for (int i = rowStart; i < rowStart+3; i++) {
                for (int j = colStart; j < colStart+3; j++) {
                    if (grid[i][j] < 1 || grid[i][j] > 9) {
                        return false;
                    }
                    rowSum[k] += grid[i][j];
                }
                if (k > 0 && rowSum[k-1] != rowSum[k]) {
                    return false;
                }
                k++;
            }
            int[] colSum = new int[3];
            k = 0;
            for (int i = colStart; i < colStart+3; i++) {
                for (int j = rowStart; j < rowStart+3; j++) {
                    colSum[k] += grid[j][i];
                }
                if (k > 0 && colSum[k-1] != colSum[k]) {
                    return false;
                }
                k++;
            }
            for (int i = 0; i < 3; i++) {
                if (rowSum[i] != colSum[i]) {
                    return false;
                }
            }
            int diagSum1 = 0;
            k = 0;
            for (int i = rowStart; i < rowStart+3; i++) {
                diagSum1 += grid[i][colStart+k];
                k++;
            }
            if (diagSum1 != rowSum[0]) {
                return false;
            }
            int diagSum2 = 0;
            k = 2;
            for (int i = rowStart+2; i >= rowStart; i--) {
                diagSum2 += grid[i][colStart+k];
                k--;
            }
            return diagSum1 == diagSum2;
        }

    }
}

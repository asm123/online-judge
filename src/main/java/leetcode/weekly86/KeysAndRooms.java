package leetcode.weekly86;

import java.util.ArrayList;
import java.util.List;

public class KeysAndRooms {

    public static void main(String[] args) {
        int[][] rooms = {{1, 3}, {3, 0, 1}, {2}, {0}};
        List<List<Integer>> roomsList = new ArrayList<>();
        for (int i = 0; i < rooms.length; i++) {
            List<Integer> temp = new ArrayList<>();
            for (int j = 0; j < rooms[i].length; j++) {
                temp.add(rooms[i][j]);
            }
            roomsList.add(temp);
        }
        System.out.println(new Solution().canVisitAllRooms(roomsList));
    }

    static class Solution {
        public boolean canVisitAllRooms(List<List<Integer>> rooms) {
            boolean[] visited = new boolean[rooms.size()];
            dfs(0, rooms, visited);
            for (boolean v : visited) {
                if (!v) {
                    return false;
                }
            }
            return true;
        }

        private void dfs(int current, List<List<Integer>> adj, boolean[] visited) {
            visited[current] = true;
            for (int i = 0; i < adj.get(current).size(); i++) {
                int next = adj.get(current).get(i);
                if (next != current && !visited[next]) {
                    dfs(next, adj, visited);
                }
            }
        }

    }
}

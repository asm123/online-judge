package leetcode.weekly93;

import java.util.HashSet;
import java.util.Set;

public class ReorderedPowerOfTwo {

    public static void main(String[] args) {
        int N = 24;
        System.out.println(new Solution().reorderedPowerOf2(N));
    }

    static class Solution {
        public boolean reorderedPowerOf2(int N) {
            Set<Integer> twoPowers = new HashSet<>();
            int i = 1;
            while (i > 0) {
                twoPowers.add(i);
                i <<= 1;
            }
            if (twoPowers.contains(N)) {
                return true;
            }
            String n = String.valueOf(N);
            int[] aMap = new int[10];
            for (int j = 0; j < n.length(); j++) {
                aMap[n.charAt(j)-'0'] += 1;
            }
            for (int p: twoPowers) {
                int[] pMap = new int[10];
                String pString = String.valueOf(p);
                for (int j = 0; j < pString.length(); j++) {
                    pMap[pString.charAt(j)-'0'] += 1;
                }
                boolean matches = true;
                for (int j = 0; j < pMap.length; j++) {
                    if (aMap[j] != pMap[j]) {
                        matches = false;
                        break;
                    }
                }
                if (matches) {
                    return true;
                }
            }
            return false;
        }
    }
}

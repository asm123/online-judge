package leetcode.weekly93;

import java.util.Arrays;
import java.util.TreeMap;

public class AdvantageShuffle {
    public static void main(String[] args) {
        int[] A = {2, 0, 4, 1, 2};
        int[] B = {1, 3, 0, 0, 2};
        System.out.println(Arrays.toString(new Solution().advantageCount(A, B)));
    }

    static class Solution {
        public int[] advantageCount(int[] A, int[] B) {
            TreeMap<Integer, Integer> count = new TreeMap<>();
            for (int a : A) {
                count.putIfAbsent(a, 0);
                count.put(a, count.get(a) + 1);
            }
            int[] perm = new int[A.length];
            for (int i = 0; i < B.length; i++) {
                Integer next = count.higherKey(B[i]);
                if (next == null) {
                    perm[i] = count.firstKey();
                } else {
                    perm[i] = next;
                }
                int c = count.get(perm[i]);
                if (c == 1) {
                    count.remove(perm[i]);
                } else {
                    count.put(perm[i], c - 1);
                }
            }
            return perm;
        }
    }
}

package leetcode.weekly80;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class AmbiguousCoordinates {

    public static void main(String[] args) {
        String s = "(0010)";
        System.out.println(new Solution().ambiguousCoordinates(s));
    }

    static class Solution {
        public List<String> ambiguousCoordinates(String S) {
            String s = S.substring(1, S.length() - 1);
            List<String> coordinates = new ArrayList<>();
            List<Pair> pairs = getAllPairs(s);
            for (Pair pair : pairs) {
                List<String> comb1 = getAllCombinations(pair.getFirst());
                List<String> comb2 = getAllCombinations(pair.getSecond());

                if (comb1.isEmpty() || comb2.isEmpty()) {
                    continue;
                }
                for (String aComb1 : comb1) {
                    for (String aComb2 : comb2) {
                        coordinates.add("(" + aComb1 + ", " + aComb2 + ")");
                    }
                }
            }

            return coordinates;
        }

        private List<Pair> getAllPairs(String s) {
            List<Pair> pairs = new ArrayList<>();
            for (int j = 1; j < s.length(); j++) {
                String first = s.substring(0, j).trim();
                String second = s.substring(j).trim();
                if (first.isEmpty() || second.isEmpty()) {
                    continue;
                }
                Pair pair = new Pair(first, second);
                pairs.add(pair);
            }
            return pairs;
        }

        private List<String> getAllCombinations(String s) {
            List<String> combinations = new ArrayList<>();
            if (s.length() == 1) {
                combinations.add(s);
            } else if (!hasOnlyZeros(s)) {
                if (s.charAt(0) == '0') {
                    if (s.charAt(s.length() - 1) != '0') {
                        combinations.add("0." + s.substring(1));
                    }
                } else {
                    combinations.add(s);
                    if (!hasTrailingZeros(s)) {
                        for (int i = 0; i < s.length() - 1; i++) {
                            combinations.add(s.substring(0, i + 1) + "." + s.substring(i + 1));
                        }
                    }
                }
            }
            return combinations;
        }

        private boolean hasOnlyZeros(String s) {
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) != '0') {
                    return false;
                }
            }
            return true;
        }

        private boolean hasTrailingZeros(String s) {
            return s.charAt(s.length() - 1) == '0';
        }

        class Pair {
            private String first;
            private String second;

            Pair(String first, String second) {
                this.first = first;
                this.second = second;
            }

            String getFirst() {
                return this.first;
            }

            String getSecond() {
                return this.second;
            }
        }
    }
}

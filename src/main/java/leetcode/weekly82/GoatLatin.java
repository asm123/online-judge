package leetcode.weekly82;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GoatLatin {

    public static void main(String[] args) {
        String s = "The quick brown fox jumped over the lazy dog";
        System.out.println(new Solution().toGoatLatin(s));
    }

    static class Solution {

        Character[] vowels = {'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U'};
        List<Character> l = Arrays.asList(vowels);
        Set<Character> vowelSet = new HashSet<>(l);

        public String toGoatLatin(String S) {
            String[] words = S.split("\\s+");
            System.out.println(Arrays.toString(words));
            for (int i = 0; i < words.length; i++) {
                String original = words[i];
                if (original.isEmpty()) {
                    continue;
                }
                char start = original.charAt(0);
                StringBuilder sb = new StringBuilder();
                if (isVowel(start)) {
                    sb.append(original);
                } else {
                    if (original.length() > 1) {
                        sb.append(original.substring(1)).append(start);
                    } else {
                        sb.append(original);
                    }
                }
                sb.append("ma");
                for (int j = 0; j <= i; j++) {
                    sb.append("a");
                }
                words[i] = sb.toString();
            }
            StringBuilder sentence = new StringBuilder();
            for (int i = 0; i < words.length; i++) {
                if (words[i].isEmpty()) {
                    continue;
                }
                if (sentence.length() > 0) {
                    sentence.append(" ");
                }
                sentence.append(words[i]);
            }
            return sentence.toString();
        }

        private boolean isVowel(char c) {
            return vowelSet.contains(c);
        }
    }
}

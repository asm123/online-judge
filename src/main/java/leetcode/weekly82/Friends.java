package leetcode.weekly82;

import java.util.*;

public class Friends {

    public static void main(String[] args) {
        int[] ages = {16, 16};
        System.out.println(new Solution().numFriendRequests(ages));
    }

    static class Solution {
        public int numFriendRequests(int[] ages) {
            int count = 0;
            int[] ageCount = new int[121];
            for (int age: ages) {
                ageCount[age]++;
            }
            for (int i = 0; i < ageCount.length; i++) {
                for (int j = 0; j < ageCount.length; j++) {
                    if (canFriend(i, j)) {
                        count += ageCount[i] * ageCount[j];
                        if (i == j) {
                            count -= ageCount[i];
                        }
                    }
                }
            }
            return count;
        }

        private boolean canFriend(int A, int B) {
            if (2 * B - A <= 14) {
                return false;
            }
            if (B > A) {
                return false;
            }
            if (B > 100 && A < 100) {
                return false;
            }
            return true;
        }
    }
}

package leetcode.weekly92;

import java.util.Arrays;

public class TransposeMatrix {

    public static void main(String[] args) {
        int[][] A = {{1,2,3},{4,5,6}, {7,8,9}};
        int[][] t = new Solution().transpose(A);
        for (int i = 0; i < t.length; i++) {
            System.out.println(Arrays.toString(t[i]));
        }
    }

    static class Solution {
        public int[][] transpose(int[][] A) {
            if (A.length == 0) {
                return new int[0][];
            }
            int rows = A.length;
            int cols = A[0].length;
            int[][] t = new int[cols][rows];

            for (int i = 0; i < cols; i++) {
                for (int j = 0; j < rows; j++) {
                    t[i][j] = A[j][i];
                }
            }

            return t;
        }
    }
}

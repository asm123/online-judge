package leetcode.weekly92;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class SmallestSubtreeWithDeepestNodes {


    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return "" + val;
        }
    }

    public static void main(String[] args) {
        String nodes = "3,5,1,6,2,0,8,null,null,7,4";
        TreeNode root = new Test().deserialize(nodes);
        TreeNode deepest = new Solution().subtreeWithAllDeepest(root);
        if (deepest != null) {
            System.out.println(deepest.val);
        } else {
            System.out.println("null");
        }

    }

    static class Test {
        public TreeNode deserialize(String data) {
            if (data.isEmpty()) {
                return null;
            }
            String[] nodesStr = data.split(",");
            TreeNode[] nodes = new TreeNode[nodesStr.length];
            for (int i = 0; i < nodesStr.length; i++) {
                String ns = nodesStr[i];
                if (!ns.equals("null")) {
                    nodes[i] = new TreeNode(Integer.parseInt(ns));
                }
            }
            int r = 0, c = 1;
            while (c < nodes.length) {
                TreeNode node = nodes[r];
                if (node != null) {
                    node.left = nodes[c];
                    node.right = nodes[c+1];
                    c += 2;
                }
                r++;

            }
            return nodes[0];
        }

        public String serialize(TreeNode root) {
            if (root == null) {
                return "null";
            }
            StringBuilder sb = new StringBuilder();
            Queue<TreeNode> q = new LinkedList<>();
            q.add(root);
            while (!q.isEmpty()) {
                TreeNode node = q.poll();
                if (node == null) {
                    sb.append("null");
                } else {
                    sb.append(node.val);
                    q.add(node.left);
                    q.add(node.right);
                }
                sb.append(",");
            }
            return sb.toString().trim();
        }

    }

    static class Solution {
        public TreeNode subtreeWithAllDeepest(TreeNode root) {
            if (root == null) {
                return null;
            }
            return deepestAncestor(root, new HashMap<TreeNode, Integer>(), 0);
        }

        private TreeNode deepestAncestor(TreeNode root, Map<TreeNode, Integer> depth, int currentDepth) {
            if (root.left == null && root.right == null) {
                depth.put(root, currentDepth);
                return root;
            }
            if (root.right == null) {
                return deepestAncestor(root.left, depth, currentDepth+1);
            }
            if (root.left == null) {
                return deepestAncestor(root.right, depth, currentDepth+1);
            }
            TreeNode leftDeepest = deepestAncestor(root.left, depth, currentDepth+1);
            TreeNode rightDeepest = deepestAncestor(root.right, depth, currentDepth+1);

            if (depth.get(leftDeepest) == depth.get(rightDeepest)) {
                depth.put(root, depth.get(leftDeepest));
                return root;
            } else if (depth.get(leftDeepest) > depth.get(rightDeepest)) {
                return leftDeepest;
            }
            return rightDeepest;
        }
    }
}

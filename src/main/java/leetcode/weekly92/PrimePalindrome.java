package leetcode.weekly92;

public class PrimePalindrome {

    public static void main(String[] args) {
        int N = 51633903;
        System.out.println(new Solution().primePalindrome(N));
    }

    static class Solution {
        public int primePalindrome(int N) {
            if (N <= 2) {
                return 2;
            }
            if (N % 2 == 0) {
                N++;
            }

            for (int i = N; i < Integer.MAX_VALUE; i++) {
                if (isPalindrome(i) && isPrime(i)) {
                    return i;
                }
            }
            return -1;
        }


        private boolean isPrime(int n) {
            if (n < 2) {
                return false;
            }
            if (n == 2) {
                return true;
            }
            if (n % 2 == 0) {
                return false;
            }
            for (int i = 3; i * i <= n; i += 2) {
                if (n % i == 0) {
                    return false;
                }
            }
            return true;
        }

        private boolean isPalindrome(int N) {
            String s = Integer.toString(N);
            int i = 0, j = s.length() - 1;
            while (i < j) {
                if (s.charAt(i) != s.charAt(j)) {
                    return false;
                }
                i++;
                j--;
            }
            return true;
        }
    }
}

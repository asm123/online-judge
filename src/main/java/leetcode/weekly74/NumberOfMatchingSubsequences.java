package leetcode.weekly74;

public class NumberOfMatchingSubsequences {

    public static void main(String[] args) {
        Solution solution = new Solution();
        String S = "abcde";
        String[] words = {"a", "bb", "acd", "ace"};
        int count = solution.numMatchingSubseq(S, words);
        System.out.println(count);
    }

    static class Solution {
        public int numMatchingSubseq(String S, String[] words) {
            int count = 0;
            for (int i = 0; i < words.length; i++) {
                if (isSubsequence(S, words[i])) {
                    count++;
                }
            }
            return count;
        }

        private boolean isSubsequence(String S, String W) {
            int start = 0;
            for (int i = 0; i < W.length(); i++) {
                boolean found = false;
                for (int j = start; j < S.length(); j++) {
                    if (S.charAt(j) == W.charAt(i)) {
                        found = true;
                        start = j+1;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }
            return true;
        }
    }

}


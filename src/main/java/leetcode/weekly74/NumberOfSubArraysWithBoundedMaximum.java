package leetcode.weekly74;

import java.util.Set;
import java.util.TreeSet;

public class NumberOfSubArraysWithBoundedMaximum {

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] a = {73, 55, 36, 5, 55, 14, 9, 7, 72, 52};
        int L = 32;
        int R = 69;
        int count = solution.numSubarrayBoundedMax(a, L, R);
        System.out.println(count);
    }

}

class Solution {
    public int numSubarrayBoundedMax(int[] A, int L, int R) {
        TreeSet<Integer> notSatisfying = new TreeSet<>();
        for (int i = 0; i < A.length; i++) {
            if (A[i] > R) {
                notSatisfying.add(i);
            }
        }

        int count = 0;
        int smallerCount = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] > R) {
                smallerCount = 0;
                continue;
            }
            if (A[i] >= L && A[i] <= R) {
                Integer nextLargestIndex = notSatisfying.higher(i);
                if (nextLargestIndex == null) {
                    nextLargestIndex = A.length;
                }
                int localCount = nextLargestIndex - i;
                if (smallerCount > 0) {
                    localCount *= (smallerCount + 1);
                    smallerCount = 0;
                }
                count += localCount;
            } else if (A[i] < L) {
                smallerCount++;
            }
        }
        return count;
    }
}

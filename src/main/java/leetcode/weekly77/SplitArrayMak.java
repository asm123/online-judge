package leetcode.weekly77;

import java.util.ArrayList;

public class SplitArrayMak {

    public static void main(String[] args) {
        int[] A = new int[]{2, 12, 18, 16, 19, 3};
        long currentTime = System.currentTimeMillis();
        System.out.println(new Solution().splitArraySameAverage(A));
        long elapsed = System.currentTimeMillis() - currentTime;
        System.out.println("Time: " + (elapsed / 1000));
    }

    static class Solution {


        public boolean isSameAvg(ArrayList<Integer> l1, ArrayList<Integer> l2) {

            if (l1.size() == 0 || l2.size() == 0)
                return false;

            float avgl1 = 0.0f;
            float avgl2 = 0.0f;

            for (int i : l1) {
                avgl1 += i;
            }
            avgl1 /= l1.size();
            for (int i : l2) {
                avgl2 += i;
            }
            avgl2 /= l2.size();

            return avgl1 == avgl2;
        }

        public boolean splitArraySameAverageR(int[] A, int index, ArrayList<Integer> l1, ArrayList<Integer> l2) {
            if (index == A.length) {
                return isSameAvg(l1, l2);
            }

//            System.out.println("L1: " + l1);
//            System.out.println("L2: " + l2);
//
//            System.out.println("-----------------------------------");

            l1.add(A[index]);
            boolean list1 = splitArraySameAverageR(A, index + 1, l1, l2);
            l1.remove(l1.size() - 1);

            l2.add(A[index]);
            boolean list2 = splitArraySameAverageR(A, index + 1, l1, l2);
            l2.remove(l2.size() - 1);

            return list1 || list2;
        }

        public boolean splitArraySameAverage(int[] A) {
            ArrayList<Integer> l1 = new ArrayList<>();
            ArrayList<Integer> l2 = new ArrayList<>();
            return splitArraySameAverageR(A, 0, l1, l2);
        }

    }
}

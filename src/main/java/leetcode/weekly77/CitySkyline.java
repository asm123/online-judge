package leetcode.weekly77;

import java.util.Arrays;

public class CitySkyline {

    public static void main(String[] args) {
        int[][] grid = new int[][] {{3,0,8,4},{2,4,5,7},{9,2,6,3},{0,3,1,0}};
        System.out.println(new Solution().maxIncreaseKeepingSkyline(grid));
    }

    static class Solution {
        public int maxIncreaseKeepingSkyline(int[][] grid) {
            int sum = 0;
            int[] left = new int[grid.length];
            int[] top = new int[grid[0].length];

            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    left[i] = Math.max(left[i], grid[i][j]);
                    top[j] = Math.max(top[j], grid[i][j]);
                }
            }
            System.out.println("Left: " + Arrays.toString(left));
            System.out.println("Top: " + Arrays.toString(top));
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    sum += Math.min(left[i], top[j]) - grid[i][j];
                }
            }
            return sum;
        }
    }
}

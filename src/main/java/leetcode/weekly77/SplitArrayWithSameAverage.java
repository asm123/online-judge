package leetcode.weekly77;

import java.util.Arrays;

public class SplitArrayWithSameAverage {

    public static void main(String[] args) {
        int[] A = new int[]{6,8,18,3,1};
        long currentTime = System.currentTimeMillis();
        System.out.println(new Solution().splitArraySameAverage(A));
        long elapsed = System.currentTimeMillis() - currentTime;
        System.out.println("Time: " + (elapsed / 1000));
    }

    static class Solution {
        public boolean splitArraySameAverage(int[] A) {
            return splitArrayRecursive(A, Arrays.stream(A).sum(), 0, 0, 0);
        }

        private boolean splitArrayRecursive(int[] A, int arraySum, int bSum, int bCount, int i) {
            if (i == A.length) {
                if (bCount == 0 || bCount == A.length) {
                    return false;
                }
                double bAvg = (double) (bSum) / bCount;
                return bAvg == (double)(arraySum - bSum) / (A.length - bCount);
            }
            return splitArrayRecursive(A, arraySum, bSum, bCount, i + 1)
                    || splitArrayRecursive(A, arraySum, bSum + A[i], bCount + 1, i + 1);
        }

        private boolean splitArrayDP(int[] A) {
            int arraySum = Arrays.stream(A).sum();





            return false;
        }

    }
}

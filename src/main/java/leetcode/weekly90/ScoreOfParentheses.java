package leetcode.weekly90;

public class ScoreOfParentheses {

    public static void main(String[] args) {
        String s = "()(())";
        System.out.println(new Solution().scoreOfParentheses(s));
    }

    static class Solution {
        public int scoreOfParentheses(String S) {
            if (S == null || S.isEmpty()) {
                return 0;
            }
            if (S.charAt(0) == '(' && S.charAt(1) == ')') {
                return 1 + scoreOfParentheses(S.substring(2));
            }
            int open = 0;
            int closed = 0;
            for (int i = 0; i < S.length(); i++) {
                if (S.charAt(i) == '(') {
                    open++;
                } else {
                    open--;
                }
                if (open == 0) {
                    closed = i+1;
                    break;
                }
            }
            return 2 * scoreOfParentheses(S.substring(1, closed-1)) + scoreOfParentheses(S.substring(closed));
        }
    }
}

package leetcode.weekly75;

import java.util.Arrays;

public class SmallestRotationWithHighestScore {

    public static void main(String[] args) {
        int[] A = {2, 3, 1, 4, 0};
        System.out.println(new Solution().bestRotation(A));
    }

    static class Solution {

        public int bestRotation(int[] A) {
            int[] score = new int[A.length];
            for (int i = 0; i < A.length; i++) {
                int left = (i - A[i] + 1 + A.length) % A.length;
                int right = (i + 1) % A.length;
                score[left]--;
                score[right]++;
                if (left > right) {
                    score[0]--;
                }
            }
            System.out.println(Arrays.toString(score));
            return 0;
        }
    }
}

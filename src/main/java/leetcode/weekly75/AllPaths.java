package leetcode.weekly75;

import java.util.*;

public class AllPaths {

    public static void main(String[] args) {
        int[][] graph = new int[][] {{1, 2}, {3}, {3}, {}};
        Solution solution = new Solution();
        List<List<Integer>> allPaths = solution.allPathsSourceTarget(graph);
        for (List<Integer> path: allPaths) {
            System.out.println(Arrays.toString(path.toArray()));
        }
    }

    static class Solution {
        public List<List<Integer>> allPathsSourceTarget(int[][] graph) {
            List<List<Integer>> allPaths = new ArrayList<>();
            int n = graph.length;
            Queue<List<Integer>> queue = new LinkedList<>();

            List<Integer> path = new ArrayList<>();
            path.add(0);
            queue.add(path);

            while (!queue.isEmpty()) {
                List<Integer> currentPath = queue.poll();
                int lastVertex = currentPath.get(currentPath.size()-1);
                if (lastVertex == n-1) {
                    allPaths.add(currentPath);
                } else {
                    for (int i = 0; i < graph[lastVertex].length; i++) {
                        if (!currentPath.contains(graph[lastVertex][i])) {
                            List<Integer> nextPath = new ArrayList<>();
                            nextPath.addAll(currentPath);
                            nextPath.add(graph[lastVertex][i]);
                            queue.add(nextPath);
                        }
                    }
                }
            }

            return allPaths;
        }
    }
}

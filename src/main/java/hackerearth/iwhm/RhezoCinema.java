package hackerearth.iwhm;



import java.util.Scanner;

class RhezoCinema {
    static long[][] combinations = new long[11][11];
    
    private static void precompute() {
    	for (int i = 1; i < 11; i++) {
    		for (int j = i; j >= 0; j--) {
    			if (i == j || j == 0) {
    				combinations[i][j] = 1;
    			}
    			else {
    				combinations[i][j] = combinations[i-1][j-1] + combinations[i-1][j];
    			}
    		}
    	}
    }
    
    public static void main(String[] args) {
    	precompute();
    	Scanner scanner = new Scanner(System.in);
    	int N, M, K;
    	N = scanner.nextInt();
    	M = scanner.nextInt();
    	K = scanner.nextInt();
    	int max = (int)Math.ceil(1.0 * N / 2);
    	long count = 0;
    	int modulo = 1000000007;
    	for (int i = 2; i <= max; i++) {
    		count = (count + combinations[max][i]) % modulo;
    	}
    	count = (N * count) % modulo;
    	System.out.println(count);
    }
}

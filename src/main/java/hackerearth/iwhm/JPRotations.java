package hackerearth.iwhm;




import java.util.Scanner;


public class JPRotations {
	
    int requiredOffset;
    int offsetSoFar = 0;
    int N;
	
    public JPRotations(int N, int offset) {
        this.N = N;
            this.requiredOffset = offset;
    }

    public boolean rotate(char direction, int rotateBy) {
        if (direction == 'R') {
            offsetSoFar += rotateBy;
        }
        else {
                offsetSoFar += -rotateBy;

                if (offsetSoFar < 0) {
                    offsetSoFar += N;
                }

        }
        offsetSoFar = offsetSoFar % N;
//        System.out.println("Required: " + requiredOffset + ", so far: " + offsetSoFar);
        return offsetSoFar == requiredOffset;
    }
	
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N, M;
        N = scanner.nextInt();
        M = scanner.nextInt();
        
        int[] original = new int[N];
        int[] target = new int[N];
        for (int i = 0; i < N; i++) {
        	original[i] = scanner.nextInt();
        }
        for (int i = 0; i < N; i++) {
        	target[i] = scanner.nextInt();
        }
        int offset = 0;
        for (int i = 0; i < N; i++) {
    		if (original[0] == target[i]) {
    			offset = i;
    			break;
    		}
    	}
        
        
        JPRotations solution = new JPRotations(N, offset);
        boolean found = false;
        int answer = -1;
        for (int i = 0; i < M; i++) {
        	String line = scanner.next().toUpperCase();
        	char direction = line.charAt(0);
        	int rotateBy = scanner.nextInt();
        	if (!found) {
                if (solution.rotate(direction, rotateBy)) {
                    answer = i+1;
                    found = true;
                }	
        	}
        }
    	System.out.println("" + answer);
    }
}


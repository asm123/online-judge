package hackerearth.jancircuits18;

import java.util.Scanner;

public class CongruenceRelation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();

        int rep = n / k;
        int extra = n % k;

        long count = (long) extra * rep * (rep + 1) / 2 + (long) (k - extra) * rep * (rep - 1) / 2;
        System.out.println(count);
    }

}

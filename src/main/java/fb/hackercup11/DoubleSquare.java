package fb.hackercup11;

import java.io.*;
import java.util.Scanner;

public class DoubleSquare {

    public static void main(String[] args) {
        File input = new File("/home/asmita/study/codes/java/CompetitiveProgramming/src/fb/hackercup11/double_squares.txt");
        File output = new File(input.getAbsolutePath() + ".output");
        Scanner scanner = null;
        try {
            scanner = new Scanner(input);
            PrintWriter writer = new PrintWriter(new FileWriter(output));
            int N = scanner.nextInt();
            for (int n = 0; n < N; n++) {
                int X = scanner.nextInt();
                System.out.println(X);
                int ways = getWaysToDoubleSquare(X);
                String outputLine = "Case #" + (n + 1) + ": " + ways;
                System.out.println(outputLine);
                writer.println(outputLine);
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int getWaysToDoubleSquare(int X) {
        int count = 0;
        for (int i = 0; i <= X / 2; i++) {
            int j = 0;
            int sumOfSquares = 0;
            while (j <= i && (sumOfSquares = (int) (Math.pow(i, 2) + (int) Math.pow(j, 2))) < X) {
                j++;
            }
            if (sumOfSquares == X) {
                count++;
            }
        }
        return count;
    }

}

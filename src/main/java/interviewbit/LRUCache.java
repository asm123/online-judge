package interviewbit;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {
    public static void main(String[] args) {
        String input = "2 2 G 2 S 2 6 G 1 S 1 5 S 1 2 G 1 G 2";
        String[] parts = input.split("\\s+");
        int N = Integer.parseInt(parts[0]);
        int capacity = Integer.parseInt(parts[1]);
        Solution solution = new Solution(capacity);
        for (int i = 2; i < parts.length; ) {
            char op = parts[i++].charAt(0);
            if (op == 'S') {
                int key = Integer.parseInt(parts[i++]);
                int value = Integer.parseInt(parts[i++]);
                solution.set(key, value);
            } else {
                int key = Integer.parseInt(parts[i++]);
                System.out.println(solution.get(key));
            }
            solution.print();
        }
    }

    static class Solution {

        private Map<Integer, Node> map;
        private int capacity;
        private Node head;
        private Node tail;


        public void print() {
            Node temp = head;
            System.out.print("[Start] ");
            while (temp != null) {
                System.out.print("(" + temp.getKey() + " -> " + temp.getValue() + ") ");
                temp = temp.getNext();
            }
            System.out.println("[End]");
        }

        public Solution(int capacity) {
            this.capacity = capacity;
            this.map = new HashMap<>();
        }

        public int get(int key) {
            Node node = map.get(key);
            if (node == null) {
                return -1;
            }
            moveNode(node);
            return node.getValue();
        }

        public void set(int key, int value) {
            Node node = map.get(key);
            if (node == null) {
                node = new Node();
                node.setKey(key);
                node.setValue(value);
                addNode(node);
            } else {
                node.setValue(value);
                moveNode(node);
            }
            map.put(key, node);
            if (map.size() > capacity) {
                evictLast();
            }

        }

        private void evictLast() {
            if (head != null) {
                if (head.getNext() != null) {
                    head.getNext().setPrevious(null);
                }
                map.remove(head.getKey());
                head = head.getNext();
            }
        }

        private void addNode(Node node) {
            if (head == null) {
                head = node;
            } else {
                tail.setNext(node);
                node.setPrevious(tail);
                node.setNext(null);
            }
            tail = node;
        }

        private void moveNode(Node node) {
            if (tail == node) {
                return;
            }
            if (head == node) {
                head = head.getNext();
                head.setPrevious(null);
            } else {
                if (node.getPrevious() != null) {
                    node.getPrevious().setNext(node.getNext());
                }
                if (node.getNext() != null) {
                    node.getNext().setPrevious(node.getPrevious());
                }
            }
            tail.setNext(node);
            node.setPrevious(tail);
            node.setNext(null);

            tail = node;
        }

        class Node {
            private int key, value;
            private Node next, previous;

            public int getKey() {
                return this.key;
            }

            public void setKey(int key) {
                this.key = key;
            }

            public int getValue() {
                return this.value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            public Node getNext() {
                return next;
            }

            public void setNext(Node next) {
                this.next = next;
            }

            public Node getPrevious() {
                return previous;
            }

            public void setPrevious(Node previous) {
                this.previous = previous;
            }
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap.aug7;

import java.util.*;

class Graph {
    int V;
    Map<Integer, List<Integer>> adjacency;

    public Graph(int V) {
        this.V = V;
        adjacency = new HashMap<Integer, List<Integer>>();
        for (int i = 1; i <= V; i++) {
            adjacency.put(i, new ArrayList<Integer>());
        }
    }
    
    public void addEdge(int u, int v) {
        adjacency.get(u).add(v);
        adjacency.get(v).add(u);
    }
    
    public List<Integer> findPath(int u, int v) {
        List<Integer> path = new ArrayList<Integer>();
        
        
        return path;
    }
    
    private void dfsUtil(int u, int v, boolean[] visited) {
        
    }
    
    public void dfs(int src, int dest) {
        boolean[] visited = new boolean[V];
        Arrays.fill(visited, false);
        dfsUtil(src, dest, visited);
    }
}



public class Solution {
	
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] a = new int[N+1];
        for (int i = 1; i <= N; i++) {
            a[i] = scanner.nextInt();
        }
        Graph g = new Graph(N);
        for (int i = 0; i < N-1; i++) {
            int u = scanner.nextInt();
            int v = scanner.nextInt();
            g.addEdge(u, v);
        }
        int Q = scanner.nextInt();
        for (int q = 0; q < Q; q++) {
            int u = scanner.nextInt();
            int v = scanner.nextInt();
            List<Integer> path = g.findPath(u, v);
            int[] count = new int[1000];
            for (int i = 0; i < path.size(); i++) {
            	count[path.get(i)]++;
            }
            int c = 0;
            for (int i = 0; i < count.length; i++) {
            	if (count[i] == 1) {
            		c++;
            	}
            }
            System.out.println(c);
        }
    }
}

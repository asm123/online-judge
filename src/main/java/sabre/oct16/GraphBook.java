/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sabre.oct16;

/**
 *
 * @author asmita
 */
import java.util.*;

class Graph {
	int V;
	
	ArrayList<Integer>[] adj;
	Set<Integer> cut;
	boolean[] visited;
	int[] parent;
	int[] discTime, earliestTime;
	int time = 0;
        int count = 0;
	
	public Graph(int V) {
		this.V = V;
		adj = (ArrayList<Integer>[])new ArrayList[this.V];
		for (int i = 0; i < this.V; i++) {
			adj[i] = new ArrayList<Integer>();
		}
		cut = new HashSet<Integer>();
		visited = new boolean[this.V];
                parent = new int[this.V];
		discTime = new int[this.V];
		earliestTime = new int[this.V];
	}
	
	public void addEdge(int u, int v) {
		adj[u].add(v);
		adj[v].add(u);
	}
	
	private void findCutVertices() {
                for (int i = 0; i < V; i++) {
                        parent[i] = -1;
                        visited[i] = false;
                }
		for (int i = 0; i < V; i++) {
                    if (!visited[i]) {
                            checkIfCutVertex(i);
                    }
		}
	}
	
        private boolean isGraphConnected() {
            for (int i = 0; i < V; i++) {
                visited[i] = false;
            }
            for (int v = 0; v < V; v++) {
                if (!visited[v]) {
                    dfs(v);
                    count++;
                }
                if (count > 1) {
                    return false;
                }
            }
            return true;
        }
        
        private void dfs(int u) {
            visited[u] = true;
            for (Integer v: adj[u]) {
                if (!visited[v]) {
                    dfs(v);
                }
            }
        }
        
	private void checkIfCutVertex(int u) {
		int numbeOfChildren = 0;
		visited[u] = true;
		
                time++;
		discTime[u] = time;
		earliestTime[u] = time;
		
		for (Integer v: adj[u]) {
			if (!visited[v]) {
                                numbeOfChildren++;
				parent[v] = u;
				checkIfCutVertex(v);
				earliestTime[u] = Math.min(earliestTime[u], earliestTime[v]);
				if ((parent[u] == -1 && numbeOfChildren > 1) 
					|| (parent[u] != -1 && earliestTime[v] >= discTime[u])) {
//                                    System.out.println("Cut vertex: " + u);
                                    cut.add(u);
				}
			}
			else {
				if (v != parent[u]) {
					earliestTime[u] = Math.min(earliestTime[u], discTime[v]);
				}
			}
		}
	}
	
	public boolean isCutVertex(int u) {
                if (!isGraphConnected()) {
                    return true;
                }
                findCutVertices();
		return cut.contains(u);
	}
}

public class GraphBook {
	
    public static void main(String[] args) {
    	Scanner scanner;
    	int N, k, x;
    	Graph g;
    	
    	scanner = new Scanner(System.in);
    	N = scanner.nextInt();
    	g = new Graph(N);
    	k = scanner.nextInt();
    	for (int i = 0; i < k; i++) {
    		g.addEdge(scanner.nextInt(), scanner.nextInt());
    	}
    	x = scanner.nextInt();
    	if (g.isCutVertex(x)) {
    		System.out.println("Not Connected");
    	}
    	else {
    		System.out.println("Connected");
    	}
    }
}

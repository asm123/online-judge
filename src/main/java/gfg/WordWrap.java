package gfg;

import java.util.Arrays;

public class WordWrap {

    public static void main(String[] args) {
        WordWrap wordWrap = new WordWrap();
        wordWrap.getMinCost(new int[] {3, 2, 2}, 4);
    }

    public void getMinCost(int[] widths, int maxWidth) {
        int[][] dp = buildAllLinesMatrix(widths, maxWidth);
        for (int i = 0; i < dp.length; i++) {
            System.out.println(Arrays.toString(dp[i]));
        }
        int[] line = new int[widths.length];
        int min = minCost(dp, line, 0, widths.length - 1);
        System.out.println("Min cost: " + min);
        for (int i = 0; i < dp.length; i++) {
            System.out.println(Arrays.toString(dp[i]));
        }
        System.out.println("Lines: " + Arrays.toString(line));

    }

    private int[][] buildAllLinesMatrix(int[] widths, int maxWidth) {
        int[][] dp = new int[widths.length][widths.length];
        for (int start = 0; start < widths.length; start++) {
            for (int end = start; end < widths.length; end++) {
                int length = 0;
                for (int i = start; i <= end; i++) {
                    int currentLength = length + widths[i];
                    if (length > 0) {
                        currentLength++;
                    }
                    if (currentLength > maxWidth) {
                        length = Integer.MAX_VALUE;
                        break;
                    }
                    length = currentLength;
                }
                if (length != Integer.MAX_VALUE) {
                    dp[start][end] = (int) Math.pow(maxWidth - length, 3);
                } else {
                    dp[start][end] = length;
                }
            }
        }
        return dp;
    }

    private int minCost(int[][] dp, int[] line, int start, int end) {
        if (start == end) {
            return dp[start][end];
        }
        if (dp[start][end] != Integer.MAX_VALUE) {
            return dp[start][end];
        }
        for (int i = start; i < end; i++) {
            int currentCost = minCost(dp, line, start, i)
                    + minCost(dp, line, i + 1, end);
            if (currentCost < dp[start][end]) {
                dp[start][end] = currentCost;
                line[end] = line[i]+1;
            } else {
                line[end] = line[i];
            }
        }
        return dp[start][end];
    }



}

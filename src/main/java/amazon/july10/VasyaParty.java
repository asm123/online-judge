/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.july10;

/**
 *
 * @author asmita
 */
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class Graph {
	Map<Integer, List<Integer>> adj;
	int N;
	boolean[] visited;
	
	public Graph(int N) {
		this.N = N;
		this.adj = new HashMap<>();
		for (int i = 1; i <= N; i++) {
			adj.put(i, new ArrayList<>());
		}
		visited = new boolean[N+1];
	}
	
	public void addEdge(int u, int v) {
		adj.get(u).add(v);
		adj.get(v).add(u);
	}
	
	private void dfs(int u, List<Integer> component) {
		visited[u] = true;
		component.add(u);
		for (Integer v: adj.get(u)) {
			if (!visited[v]) {
				dfs(v, component);
			}
		}
	}
	
	public List<List<Integer>> getConnectedComponents() {
		List<List<Integer>> components = new ArrayList<>();
		for (int i = 1; i <= N; i++) {
			visited[i] = false;
		}
		for (int i = 1; i <= N; i++) {
			if (!visited[i]) {
				List<Integer> component = new ArrayList<>();
				dfs(i, component);
				components.add(component);
			}
		}
		return components;
	}
	
}


public class VasyaParty {
	
	public static void main (String[] args) {
		Scanner scanner;
		int N, M;
		int modulo = 1000000007;
		
		scanner = new Scanner(System.in);
		N = scanner.nextInt();
		M = scanner.nextInt();
		
		Graph g = new Graph(N);
		int[] knowledge = new int[N+1];
		for (int i = 1; i <= N; i++) {
			knowledge[i] = scanner.nextInt();
		}
		
		for (int i = 0; i < M; i++) {
			int u = scanner.nextInt();
			int v = scanner.nextInt();
			g.addEdge(u, v);
		}
		long numberOfWays = 1;
		List<List<Integer>> components = g.getConnectedComponents();
		for (List<Integer> component: components) {
			int max = 0;
			for (Integer v: component) {
				max = Math.max(max, knowledge[v]);
			}
			int count = 0;
			for (Integer v: component) {
				if (knowledge[v] == max) {
					count++;
				}
			}
			numberOfWays = (numberOfWays * count) % modulo;
		}
		
		System.out.println(numberOfWays);
		
	}
	
}

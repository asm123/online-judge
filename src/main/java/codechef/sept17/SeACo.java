package codechef.sept17;

import java.util.Arrays;
import java.util.Scanner;

public class SeACo {

    private static int modulo = 1000000007;

    short[] type;
    int[] from, to;
    int[] a;
    int[] count;

    public SeACo(int n, int m) {
        a = new int[n];
        type = new short[m];
        from = new int[m];
        to = new int[m];
        count = new int[m];
    }

    public void addCommand(int index, short type, int from, int to) {
        this.type[index] = type;
        this.from[index] = from;
        this.to[index] = to;
        this.count[index] = 1;
    }

    public void updateCount() {
        for (int i = type.length-1; i >= 0; i--) {
            if (type[i] == 2) {
                for (int j = from[i]; j <= to[i]; j++) {
                    count[j] = (count[j] % modulo  + count[i] % modulo) % modulo;
                }
            }
        }
        System.out.println(Arrays.toString(count));
    }

    public void executeCommands() {
        for (int i = 0; i < type.length; i++) {
            if (type[i] == 1) {
                for (int j = from[i]; j <= to[i]; j++) {
                    a[j] = (a[j] % modulo + count[i]) % modulo;
                }
            }
        }
    }

    public void printArray() {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        for (int t = 0; t < T; t++) {
            int n = scanner.nextInt();
            int m = scanner.nextInt();

            SeACo seACo = new SeACo(n, m);

            for (int i = 0; i < m; i++) {
                short type = scanner.nextShort();
                int from = scanner.nextInt()-1;
                int to = scanner.nextInt()-1;
                seACo.addCommand(i, type, from, to);
            }
            seACo.updateCount();
            seACo.executeCommands();
            seACo.printArray();
        }
    }

}

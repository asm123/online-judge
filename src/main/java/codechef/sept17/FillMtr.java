package codechef.sept17;

import java.util.Arrays;
import java.util.Scanner;

public class FillMtr {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        for (int t = 0; t < T; t++) {
            int N = scanner.nextInt();
            int Q = scanner.nextInt();

            int[][] B = new int[N][N];
            fillMatrix(B);
            for (int q = 0; q < Q; q++) {
                int i = scanner.nextInt();
                int j = scanner.nextInt();
                int val = scanner.nextInt();
                B[i - 1][j - 1] = val;
            }
            if (isGood(B)) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
        }
    }

    private static void fillMatrix(int[][] B) {
        int secondDiagonal = B.length % 2 == 0 ? 1 : 0;
        for (int i = 0; i < B.length; i++) {
            for (int j = 0; j < B.length; j++) {
                if (i == j) {
                    B[i][j] = 0;
                } else if (j == B.length - i - 1 && secondDiagonal == 0) {
                    B[i][j] = secondDiagonal;
                } else {
                    B[i][j] = -1;
                }
            }
        }
    }

    private static boolean isGood(int[][] B) {
        for (int i = 0; i < B.length; i++) {
            for (int j = 0; j < B.length; j++) {
                if (i == j) {
                    if (B[i][j] != 0) {
                        return false;
                    }
                } else {
                    if (B[i][j] == -1 && B[j][i] == -1) {
                        continue;
                    }
                    if (B[i][j] == -1 || B[j][i] == -1) {
                        B[i][j] = Math.max(B[i][j], B[j][i]);
                        B[j][i] = B[i][j];
                    } else if (B[i][j] != B[j][i]) {
                        return false;
                    }
                }
            }
        }
        for (int i = 1; i < B.length; i++) {
            int previousI = i-1;
            int previousJ = B.length - previousI - 1;
            if (B[i][B.length-i-1] != B[previousI][previousJ]) {
                return false;
            }
        }
//        for (int i = 0; i < B.length; i++) {
//            System.out.println(Arrays.toString(B[i]));
//        }

        return true;
    }

}

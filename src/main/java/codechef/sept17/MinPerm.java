package codechef.sept17;

import java.util.Scanner;

public class MinPerm {

    private static int MAX_LENGTH = 100001;
    private static int[] permutation = new int[MAX_LENGTH];

    public static void main(String[] args) {
        populatePermutation();

        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        for (int t = 0; t < T; t++) {
            int n = scanner.nextInt();
            printPermutation(n);
        }
    }

    private static void populatePermutation() {
        for (int i = 0; i < MAX_LENGTH; i++) {
            if (i % 2 == 0) {
                permutation[i] = i + 2;
            } else {
                permutation[i] = i;
            }
        }
    }

    private static void printPermutation(int n) {
        for (int i = 0; i < n-2; i++) {
            System.out.print(permutation[i] + " ");
        }
        if (n % 2 != 0) {
            System.out.print(n + " " + permutation[n-2] + " ");
        } else {
            for (int i = n-2; i < n; i++) {
                System.out.print(permutation[i] + " ");
            }
        }
        System.out.println();
    }
}

package codechef.sept17;

import java.util.Scanner;

public class ChefpDig {

    private static void pickCharacters(String number) {
        int[] count = new int[10];
        for (int i = 0; i < number.length(); i++) {
            count[number.charAt(i)-'0']++;
        }
        int start;
        if (count[6] > 0) {
            start = 65;
            for (int i = 5; i <= 9; i++) {
                if ((i != 6 && count[i] > 0) || (i == 6 && count[i] > 1)) {
                    System.out.print((char) start);
                }
                start++;
            }
        }
        if (count[7] > 0) {
            start = 70;
            for (int i = 0; i <= 9; i++) {
                if ((i != 7 && count[i] > 0) || (i == 7 && count[i] > 1)) {
                    System.out.print((char) start);
                }
                start++;
            }
        }
        if (count[8] > 0) {
            start = 80;
            for (int i = 0; i <= 9; i++) {
                if ((i != 8 && count[i] > 0) || (i == 8 && count[i] > 1)) {
                    System.out.print((char) start);
                }
                start++;
            }
        }
        if (count[9] > 0 && count[0] > 0) {
            start = 90;
            System.out.print((char) start);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = Integer.parseInt(scanner.nextLine());
        for (int t = 0; t < T; t++) {
            String number = scanner.nextLine();
            pickCharacters(number);
        }
    }



}

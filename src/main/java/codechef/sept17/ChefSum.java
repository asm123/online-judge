package codechef.sept17;

import java.util.Scanner;

public class ChefSum {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        for (int t = 0; t < T; t++) {
            int N = scanner.nextInt();
            int[] a = new int[N];
            for (int i = 0; i < N; i++) {
                a[i] = scanner.nextInt();
            }
            System.out.println(getLeastSumIndex(a));
        }
    }

    private static int getLeastSumIndex(int[] a) {
        int minSumIndex = a.length-1;
        long minSum = Long.MAX_VALUE;
        long[] prefixSum = new long[a.length];
        long[] suffixSum = new long[a.length];

        prefixSum[0] = a[0];
        for (int i = 1; i < a.length; i++) {
            prefixSum[i] = a[i] + prefixSum[i-1];
        }

        suffixSum[a.length-1] = a[a.length-1];
        for (int i = a.length-1; i > 0; i--) {
            suffixSum[i-1] = a[i-1] + suffixSum[i];
        }

        for (int i = a.length-1; i >= 0; i--) {
            long sum = prefixSum[i] + suffixSum[i];
            if (sum <= minSum) {
                minSum = sum;
                minSumIndex = i;
            }
        }

        return minSumIndex + 1;
    }
}

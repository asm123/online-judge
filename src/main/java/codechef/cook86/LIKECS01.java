package codechef.cook86;

import java.util.Scanner;

public class LIKECS01 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = Integer.parseInt(scanner.nextLine());
        for (int t = 0; t < T; t++) {
            String s = scanner.nextLine();
            if (hasTwoEqualSubsequences(s)) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
        }
    }

    private static boolean hasTwoEqualSubsequences(String s) {
        boolean[] appeared = new boolean[26];
        for (int i = 0; i < s.length(); i++) {
            int index = s.charAt(i) - 'a';
            if (appeared[index]) {
                return true;
            } else {
                appeared[index] = true;
            }
        }
        return false;
    }

}

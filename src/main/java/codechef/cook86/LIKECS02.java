package codechef.cook86;

import java.util.Scanner;

public class LIKECS02 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        for (int t = 0; t < T; t++) {
            int N = scanner.nextInt();
            if (N <= 3) {
                for (int i = 1; i <= N; i++) {
                    System.out.print(i + " ");
                }
                System.out.println();
            } else {
                System.out.println(-1);
            }
        }
    }

}

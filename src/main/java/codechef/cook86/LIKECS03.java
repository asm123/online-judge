package codechef.cook86;

import java.util.Scanner;

public class LIKECS03 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();
        for (int t = 0; t < T; t++) {
            int N = scanner.nextInt();
            int K = scanner.nextInt();
            int[] a = new int[N];
            for (int i = 0; i < N; i++)
                a[i] = scanner.nextInt();
             System.out.println(minimumElements(a, K));
        }
     }

     private static int minimumElements(int[] a, int K) {
         int required = (int)(Math.pow(2, K)) -1 - a.length;
         return required;
     }

}

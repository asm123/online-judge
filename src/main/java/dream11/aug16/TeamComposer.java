package dream11.aug16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Solution for forming two teams of required size from an array of N players, 
 * satisfying the given constraints of maximum number of players with a certain 
 * skill in a team. <br>
 * 
 * Assumptions: <br>
 * <ol>
 * 
 * <li>Skill arrays are disjoint, i.e., no player has more than one skill 
 * (since in that case, we may have to specify strength for each skill).</li>
 * <li>A team must have at least one player with a skill (unless maximum number 
 * constraint says otherwise).</li>
 * 
 * </ol>
 * 
 * @author asmita
 */
public class TeamComposer {

    // arrays of players
    private final int[] players, strength, batsmen, bowlers, wicketKeepers;
    
    // constraints
    private int teamSize, maxBatsmen, maxBowlers, maxWicketKeepers;
    
    //
    private final int[] playerTypes;
    
    /**
     * Enumeration for player types, viz., batsman, bowler and wicket-keeper.
     */
    enum PlayerType {
        BATSMAN(0),
        BOWLER(1),
        WICKETKEEPER(2)
        ;
        
        private final int value;
        private PlayerType(int value) {
            this.value = value;
        }
        
        private int getValue() {
            return this.value;
        }
    }

    public TeamComposer(int[] players, int[] strength, int[] batsmen, int[] bowlers, int[] wicketKeepers) {
        this.players = players;
        this.strength = strength;
        this.batsmen = batsmen;
        this.bowlers = bowlers;
        this.wicketKeepers = wicketKeepers;
        
        this.playerTypes = new int[players.length];
        
        // setting up playerTypes array for the three types 
        // (required later for checking validity of teams).
        for (int i = 0; i < batsmen.length; i++) {
            playerTypes[batsmen[i]-1] = PlayerType.BATSMAN.getValue();
        }
        for (int i = 0; i < bowlers.length; i++) {
            playerTypes[bowlers[i]-1] = PlayerType.BOWLER.getValue();
        }
        for (int i = 0; i < wicketKeepers.length; i++) {
            playerTypes[wicketKeepers[i]-1] = PlayerType.WICKETKEEPER.getValue();
        }
    }
    
    public void setConstraints(int teamSize, int maxBatsmen, int maxBowlers, int maxWicketKeepers) {
        this.teamSize = teamSize;
        this.maxBatsmen = maxBatsmen;
        this.maxBowlers = maxBowlers;
        this.maxWicketKeepers = maxWicketKeepers;
    }
    
    /**
     * Prints the two teams (if could be formed), their total strengths
     * and absolute difference between their strengths.
     */
    public void formTeams() {
        if (teamSize * 2 > players.length) {
            System.out.println("Error: Insufficient players (" + players.length 
                    + ") to form two teams " + "of size " + teamSize + ".");
            return;
        }
        
        int[] team1, team2;
        
        List<List<Integer>> possibleTeams;
        List<Integer> temp;
        
        int minDiff = Integer.MAX_VALUE;
        int maxTotalStrength = 0;
        int diff;
        int strongestI = -1, strongestJ = -1;
        int strengthOfI, strengthOfJ, totalStrength;
        
        // find all possible "valid" combinations of players
        possibleTeams = new ArrayList<>();
        getAllPossibleValidTeams(players.length, teamSize, new Integer[teamSize], 0, 0, possibleTeams);
        
        if (possibleTeams.isEmpty()) {
            System.out.println("Teams cannot be formed with the specified constraints.");
            return;
        }
        
        temp = new ArrayList<>();
        
        for (int i = 0; i < possibleTeams.size(); i++) {
            
            strengthOfI = getTotalStrength(toArray(possibleTeams.get(i)));
            
            for (int j = i+1; j < possibleTeams.size(); j++) {
                
                // checking for disjointness of teams by finding intersection
                
                temp.clear();
                temp.addAll(possibleTeams.get(i));
                temp.retainAll(possibleTeams.get(j));
                if (!temp.isEmpty()) {
                    // intersection is not empty, discard the jth team
                    continue;
                }
                
                // teams are disjoint, check for max total strength and min difference in strengths
                
                strengthOfJ = getTotalStrength(toArray(possibleTeams.get(j)));
                
                totalStrength = strengthOfI + strengthOfJ;
                diff = Math.abs(strengthOfI - strengthOfJ);
                if (diff <= minDiff || totalStrength >= maxTotalStrength) {
                    minDiff = diff;
                    strongestI = i;
                    strongestJ = j;
                    maxTotalStrength = totalStrength;
                }
            }
        }
        
        team1 = toArray(possibleTeams.get(strongestI));
        team2 = toArray(possibleTeams.get(strongestJ));
        strengthOfI = getTotalStrength(team1);
        strengthOfJ = getTotalStrength(team2);
        
        System.out.println("Team 1: " + Arrays.toString(team1) + ", total strength: " + strengthOfI);
        System.out.println("Team 2: " + Arrays.toString(team2) + ", total strength: " + strengthOfJ);
        System.out.println("Absolute difference in strengths: " + minDiff);
    }

    /**
     * Finds <code>n</code> choose <code>r</code> teams 
     * and populates only valid teams in <code>allTeams</code>.
     * @param n number of players
     * @param r team size
     * @param team temporary holder of the current combination of players (team)
     * @param i index in players array
     * @param index index in current combination array
     * @param allTeams holder of all valid teams
     */
    private void getAllPossibleValidTeams(int n, int r, Integer[] team,
            int i, int index, List<List<Integer>> allTeams) {
        if (index == r) {
            // discard the team if it does not satisfy the given constraints
            if (isAValidTeam(team)) {
                allTeams.add(new ArrayList<>(Arrays.asList(team)));
            }
            return;
        }
        if (i >= n) {
            return;
        }
        team[index] = players[i];
        getAllPossibleValidTeams(n, r, team, i + 1, index + 1, allTeams);
        getAllPossibleValidTeams(n, r, team, i + 1, index, allTeams);
    }

    /**
     * Checks if the given <code>team</code> satisfies the constraints for 
     * maximum number of players with a certain skill.
     * @param team
     * @return true if team satisfies the constraints, else false.
     */
    private boolean isAValidTeam(Integer[] team) {
        int numberOfBatsmen = 0;
        int numberOfBowlers = 0;
        int numberOfWicketKeepers = 0;
        
        for (Integer teamPlayer : team) {
            if (playerTypes[teamPlayer - 1] == PlayerType.BATSMAN.getValue()) {
                numberOfBatsmen++;
            } else if (playerTypes[teamPlayer - 1] == PlayerType.BOWLER.getValue()) {
                numberOfBowlers++;
            } else if (playerTypes[teamPlayer - 1] == PlayerType.WICKETKEEPER.getValue()) {
                numberOfWicketKeepers++;
            }
        }
        
        if (numberOfBatsmen > maxBatsmen || numberOfBowlers > maxBowlers || numberOfWicketKeepers > maxWicketKeepers) {
            return false;
        }
        
        // each team must have at least one player with a skill, unless constraints say otherwise
        if ((maxBatsmen != 0 && numberOfBatsmen == 0) 
                || (maxBowlers != 0 && numberOfBowlers == 0) 
                || (maxWicketKeepers != 0 && numberOfWicketKeepers == 0)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Utility method to calculate total strength of a given team.
     * @param team
     * @return total strength of the team
     */
    private int getTotalStrength(int[] team) {
        int totalStrength = 0;
        for (int i = 0; i < team.length; i++) {
            totalStrength += strength[team[i]-1];
        }
        return totalStrength;
    }
    
    /**
     * Utility method to convert a list of integers to array.
     * @param list
     * @return array representation of the input list
     */
    private int[] toArray(List<Integer> list) {
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }
}
